﻿using IdentityServer4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.SSO
{
    public class Config
    {
        public static IEnumerable<ApiResource> GetAllApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("qwik-clix-api2")
            };
        }

        public static IEnumerable<Client> GetAllApiClients()
        {
            return new List<Client>
            {
                new Client{
                    ClientId="qwik-clix-api2",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets =
                    {
                        new Secret ("Flashlight Highlight Red Light".Sha256())
                    },
                    AllowedScopes = { "qwik-clix-api2" }

                }
            };
        }
    }
}
